using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class RagePixelCamera : MonoBehaviour {

    public int pixelSize=1;
    public int resolutionPixelWidth = 1024;
    public int resolutionPixelHeight = 768;

    public void SnapToIntegerPosition()
    {
        transform.position = new Vector3(Mathf.RoundToInt(transform.position.x) + 0.05f, Mathf.RoundToInt(transform.position.y) - 0.05f, Mathf.RoundToInt(transform.position.z) - 0.05f);
    }

    void Awake()
    {
     
    }

	void Start () {
	    
	}

    public void OnPostRender()
    {

    }

	void LateUpdate () 
    {
        SnapToIntegerPosition();
	}

    public void OnDrawGizmosSelected()
    {
        SnapToIntegerPosition();
    }

    public void ResetCamera()
    {

    }

}
