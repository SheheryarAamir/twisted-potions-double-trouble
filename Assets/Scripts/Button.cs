using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour
{

	//Storing the reference to RagePixelSprite -component
	private RagePixelSprite ragePixel;

	// Enum that manages the button status.
	public enum ButtonStatus
	{
		Normal=0,
		Pressed
	};
	
	
	
	void Start ()
	{
		ragePixel = GetComponent<RagePixelSprite> ();
	}
	
	public void Press (ButtonStatus status)
	{
		if (status == ButtonStatus.Normal)
			ragePixel.PlayNamedAnimation ("Normal", false);
		else
			ragePixel.PlayNamedAnimation ("Pressed", false);									
	}
	
}
